from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from .views import *
from django.contrib.auth.models import User

# Create your tests here.

class test(TestCase):
    def test_response(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_is_index_used(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')

    def test_response_login(self):
        response = Client().get('/log_in/')
        self.assertEqual(response.status_code, 200)

    def test_is_login_used(self):
        response = Client().get('/log_in/')
        self.assertTemplateUsed(response, 'registration/login.html')

    def test_response_register(self):
        response = Client().get('/register/')
        self.assertEqual(response.status_code, 200)

    def test_is_register_used(self):
        response = Client().get('/register/')
        self.assertTemplateUsed(response, 'registration/register.html')

    def test_register_form(self):
        response = self.client.post('/register/', data={'username': 'user', 'password1' : 'thepassword', 'password2' : 'thepassword'})
        input = User.objects.all().first()
        self.assertEqual(input.username, 'user')
