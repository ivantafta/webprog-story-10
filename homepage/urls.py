from django.urls import path
from . import views
from django.contrib.auth.views import LogoutView

app_name = 'homepage'

urlpatterns = [
	path('', views.index, name='index'),
	path('log_in/', views.log_in, name='log_in'),
	path('register/', views.register, name='register'),
	path('logout/', LogoutView.as_view(next_page='/'), name='logout'),
	# ...
]